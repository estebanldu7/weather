import React, { Component } from 'react';
import Header from './components/Header';
import Formulario from './components/Form';
import Error from "./components/Error";
import Weather from "./components/Weather";

class App extends Component {

    state = {
        error : [],
        query : {},
        resultd : {}
    };

    componentDidUpdate(prevState){
        if(prevState.query !== this.state.query){
            this.queryApi()
        }
    }

    componentDidMount(){
        this.setState(() => {
            return {error : false}
        });
    }

    queryApi = () => {
        const {city, country} = this.state.query;

        if(!city || !country){
            return null;
        }

        //query with fetch api
        const apiKey = 'a83b5ffd03fcc7a22ac7714505962efa';
        let url = `http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${apiKey}`;

        fetch(url).then(resp => {
            return resp.json()
        }).then(data => {
            this.setState(() => {
                return {resultd : data}
            });
        })
    }

    dataQuery = (response) => {

        if(response.city === '' || response.country === ''){
            this.setState(() => {
                return {error : true}
            });
        }else{
            this.setState(() => {
                return {query : response, error: false}
            });
        }
    };

    render() {
        const error = this.state.error;
        let result;

        if(error){
            result = <Error messageError = "Todos los campos son obligatorios."/>
        }else{
            result = <Weather resultd={this.state.resultd}/>
        }

        return (
            <div className="app">
                <Header titulo = 'Clima React' />
                <Formulario dataQuery={this.dataQuery} />
                {result}
            </div>
        );
    }
}

export default App;

import React, { Component } from 'react';

class Weather extends Component{

    showResult = () => {
        const {name, weather, main} = this.props.resultd;

        if(!name || !weather || !main){
            return null;
        }

        const kelvin = 273.15;
        const iconUrl = `http://openweathermap.org/img/w/${weather[0].icon}.png`;
        const alt = `clima de ${name}`;
        return(
            <div className={"row"}>
                <div className={"resultado col s12 m8 l6 offset-m2 offset-l3"}>
                    <div className={"card-panel light-blue align-center"}>
                        <h2> Resultado del clima: {name}</h2>
                        <p className={"temperatura"}>
                            Actual: {(main.temp - kelvin).toFixed(2)} &deg;C
                            <img src={iconUrl} alt={alt} />
                        </p>
                        <p>Max. {main.temp_max - kelvin} &deg;</p>
                        <p>Min. {main.temp_min - kelvin} &deg;</p>
                    </div>
                </div>
            </div>
        )
    };

    render(){
        return(
            <div className={"container"}>
                {this.showResult()}
            </div>
        )
    }
}

export default Weather;

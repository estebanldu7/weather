import React, { Component } from 'react';

class Form extends Component {

    city = React.createRef();
    country = React.createRef();

    searchWeather = (e) =>{
        e.preventDefault();

        const response = {
            country: this.country.current.value,
            city: this.city.current.value
        };

        this.props.dataQuery(response); //Send data to App.js
    };

    render() {
        return (
            <div className="contenedor-form">
                <div className="container">
                    <div className="row">
                        <form onSubmit={this.searchWeather}>
                            <div className="input-field col s12 m8 l4 offset-m2">
                                <input id="ciudad" ref={this.city} type="text" />
                                <label htmlFor="ciudad">Ciudad:</label>
                            </div>
                            <div className="input-field col s12 m8 l4 offset-m2">
                                <select ref={this.country}>
                                    <option value="" defaultValue>Elige un país</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="AR">Argentina</option>
                                    <option value="CO">Colombia</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="ES">España</option>
                                    <option value="US">Estados Unidos</option>
                                    <option value="MX">México</option>
                                    <option value="PE">Perú</option>
                                </select>
                                <label htmlFor="pais">País:</label>
                            </div>
                            <div className="input-field col s12 m8 l4 offset-2 buscador">
                                <input type="submit" className="waves-effect waves-light btn-large yellow accent-4" value="Buscar" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Form;